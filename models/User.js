const mongoose = require('mongoose')
const Schema = mongoose.Schema

// Create Schema
const UserSchema = new Schema({
  userName: {
    type: String,
    required: true
  },
  email: {
    type: String,
    required: true
  },
  password: {
    type: String,
    required: true
  },
  date: {
    type: Date,
    default: Date.now
  },
  avatar: {
    type: String
  },
  money: {
    type: Number,
    default: 200
  },
  level: {
    type: Number,
    default: 1
  },
  experience: {
    type: Number,
    default: 1
  },
  trees: [{
    tree: {
      type: Schema.Types.ObjectId,
      ref: 'Tree'
    },
    status: {
      type: String,
      default: 'new'
    },
    startTime: {
      type: Number,
      default: (new Date()).getTime()
    },
    wateringTime: {
      type: Number,
      default: 0
    }
  }]
});

module.exports = User = mongoose.model('User', UserSchema);
