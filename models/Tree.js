const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const TreeSchema = new Schema({
  name: {
    type: String,
    required: true
  },
  imagePath: {
    type: String,
    required: true
  },
  buyingPrice: {
    type: Number,
    required: true
  },
  sellingPrice: {
    type: Number,
    required: true
  },
  numberOfWatering: {
    type: Number,
    required: true
  },
  timeForWateringToTheTree: {
    type: Number,
    default: 2
  },
  isWatered: {
    type: Boolean,
    default: false
  },
  date: {
    type: Date,
    default: Date.now
  }
})

module.exports = Tree = mongoose.model('Tree', TreeSchema)