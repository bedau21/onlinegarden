const express = require('express')
const router = express.Router()
const User = require('../../models/User')
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')
const keys = require('../../config/keys')
const passport = require('passport')
const validateRegisterInput = require('../../validation/register')
const validateLoginInput = require('../../validation/login')
const Tree = require('../../models/Tree')
const CORN_TREE = 'Corn'
const MAX_TREES = 15
const WATERING_DELAY = 60000
// regiser user
router.post('/register', (req, res) => {
  const { errors, isValid } = validateRegisterInput(req.body)
  // Check validation
  if (!isValid) {
    return res.status(400).json(errors)
  }
  User.findOne({ userName: req.body.userName }).then(user => {
    if (user) {
      errors.userName = 'Username is existed'
      return res.status(400).json(errors)
    } else {
      Tree.findOne({ name: CORN_TREE })
        .then(tree => {
          let listTree = [];
          if (tree) {
            const initialTreeId = tree.id
            for (let i = 0; i < MAX_TREES; i++) {
              if (i < 5) {
                listTree.push(initialTreeId)
              }
            }
            // create newUser
            const newUser = new User({
              userName: req.body.userName,
              email: req.body.email,
              password: req.body.password,
              trees: listTree.map(tree => ({tree}))
            })
            bcrypt.genSalt(10, (err, salt) => {
              bcrypt.hash(newUser.password, salt, (err, hash) => {
                if (err) throw err;
                newUser.password = hash
                newUser
                  .save()
                  .then(user => res.json(user))
                  .catch(err => console.log(err))
              })
            })
          }
        }).catch(err => console.log(err))
    }
  })
})

// login user
router.post('/login', (req, res) => {
  const { errors, isValid } = validateLoginInput(req.body)

  // Check validation
  if (!isValid) {
    return res.status(400).json(errors)
  }

  const userName = req.body.userName
  const password = req.body.password
  User.findOne({ userName }).then(user => {
    if (!user) {
      return res.status(404).json({ userName: 'User is not exist' })
    }

    bcrypt.compare(password, user.password)
      .then(isMatch => {
        // check match
        if (isMatch) {
          const payload = { id: user.id, userName: user.userName, email: user.email }
          jwt.sign(payload, keys.secretOrKey, {
            expiresIn: 72000
          },
            (err, token) => {
              res.json({
                success: true,
                token: 'Bearer ' + token
              })
            }
          )
        } else {
          return res.status(400).json({ password: 'Password incorrect' })
        }
      })
  })
})

// delete user's tree
router.delete('/deleteTree/:userId/:index', passport.authenticate('jwt', { session: false }), (req, res) => {
  User.findById(req.params.userId)
    .then(user => {
      // Check to see if tree exists
      if (!user.trees[req.params.index]) {
        return res
          .status(404)
          .json({ treenotexists: 'Tree does not exist' });
      }
      // Splice tree out of array
      user.trees.splice(req.params.index, 1);
      user.save().then(user => res.json(user));
    })
    .catch(err => res.status(404).json({ usernotfound: 'User node found' }));
}
);

// add user's tree
router.post('/addTree', passport.authenticate('jwt', { session: false }), (req, res) => {
  User.findById(req.body.userId)
    .then(user => {
      // Add Tree
      Tree.findById(req.body.treeId).then(tree => {
        if (user.money >= tree.buyingPrice && user.trees.length < MAX_TREES) {
          user.trees.splice(req.body.index, 0, {tree})
          user.money = user.money - tree.buyingPrice
          user.save().then(user => res.json(user))
        } else {
          return res.status(404).json({ message: 'Your money is not enough, please add more money!' })
        }
      })
    })
    .catch(err => res.status(404).json({ usernotfound: 'User node found' }));
}
);

// harvestTree
router.post('/harvestTree', passport.authenticate('jwt', { session: false }), (req, res) => {
  User.findById(req.body.userId).then(user => {
      // Add Tree
      Tree.findById(req.body.treeId).then(tree => {
        if (user.trees[req.body.index].status === 'harvest') {
          user.money += tree.sellingPrice
          user.trees.splice(req.body.index, 1)
          user.save().then(user => res.json(user))
        } else {
          res.status(404).json({ message: 'Tree is not enough to harvest, watering it to make them growing more!' })
        }
      })
    })
    .catch(err => res.status(404).json({ usernotfound: 'User node found' }));
  }
);

// update user's tree status
router.post('/updateTree', passport.authenticate('jwt', { session: false }), (req, res) => {
  User.findById(req.body.userId)
    .then(user => {
      // Update tree status
      const timeWatering = Math.abs(req.body.endTime - user.trees[req.body.index].startTime)
      if (timeWatering > WATERING_DELAY) {
        user.trees[req.body.index].startTime += timeWatering;
        const treeUpdate = user.trees[req.body.index]
        treeUpdate.status = 'growing'
        treeUpdate.wateringTime++;
        if (treeUpdate.wateringTime > 2) {
          treeUpdate.status = 'harvest'
        }
        if (treeUpdate.wateringTime > 3) {
          res.status(404).json({ message: 'Tree can be harvested, it will be die if you try watering too much!'})
        }
        user.save().then(user => res.json(user.trees))
      } else {
        res.status(404).json({ message: 'You need to wait for 2 hours for the next watering!'})
      }
    })
    .catch(err => res.status(404).json({ usernotfound: 'User node found' }));
}
);

// get current user
router.get('/current',
  passport.authenticate('jwt', { session: false }),
  (req, res) => {
    res.json(req.user)
  }
)

module.exports = router;