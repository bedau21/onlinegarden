const express = require('express');
const router = express.Router();
const passport = require('passport');
const Tree = require('../../models/Tree')
const validateTreeInput = require('../../validation/tree')

// @ Get all tree
router.get('/all', (req, res) => {
  const errors = {};
  Tree.find()
    .then(trees => {
      if (!trees) {
        errors.notrees = 'There are no trees'
        res.status(404).json(errors);
      }
      res.json(trees);
    })
    .catch(err => res.status(404).json(err));
});

// Find tree by id
router.get('/:id', (req, res) => {
  const errors = {};
  Tree.findById(req.params.id)
    .then(tree => {
      if (!tree) {
        errors.notree = 'There is no tree'
        res.status(404).json(errors);
      }
      res.json(tree);
    })
    .catch(err => res.status(404).json(err));
});

// Create tree
router.post('/create', passport.authenticate('jwt', { session: false }),
  (req, res) => {
    const { errors, isValid } = validateTreeInput(req.body);
    // check validation
    if (!isValid) {
      // Return any errors with 400 status
      return res.status(400).json(errors);
    }
    // Get fields
    const newTree = new Tree({
      name: req.body.name,
      buyingPrice: req.body.buyingPrice,
      imagePath: req.body.imagePath,
      sellingPrice: req.body.sellingPrice,
      numberOfWatering: req.body.numberOfWatering,
    });
    Tree.findOne({ name: req.body.name })
      .then(tree => {
        if (tree) {
          // Update
          tree.findOneAndUpdate(
            { $set: newTree },
            { new: true }
          ).then(tree => res.json(tree));
        } else {
          // Create
          new Tree(newTree).save().then(tree => res.json(tree));
        }
      })
  }
)

// delete tree by admin
router.delete('/:id', passport.authenticate('jwt', { session: false }), (req, res) => {
  Tree.findById(req.params.id)
    .then(tree => {
      // Delete
      tree.remove().then(() => res.json({ success: true }));
    })
    .catch(err => res.status(404).json({ postnotfound: 'No post found' }));
  }
);

module.exports = router;