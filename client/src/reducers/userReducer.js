import { GET_USER_DATA, DELETE_TREE_OF_USER, 
  ADD_TREE_TO_USER, UPDATE_TREE_STATUS, HARVEST_TREE_OF_USER } from '../actions/types';
const initialState = {
  isAuthenticated: false,
  user: {},
  userData: {
    trees: []
  }
};

export default function(state = initialState, action) {
  switch (action.type) {
    case GET_USER_DATA:
      return {
        ...state,
        userData: action.payload
      };
    case ADD_TREE_TO_USER:
    return {
      ...state,
      userData: action.payload
    }
    case DELETE_TREE_OF_USER:
    return {
      ...state,
      userData: action.payload
    }
    case UPDATE_TREE_STATUS:
    return {
      ...state,
      userData: {...state.userData, 
        trees: action.payload}
    }
    case HARVEST_TREE_OF_USER:
    return {
      ...state,
      userData: action.payload
    }
    default:
      return state;
  }
}