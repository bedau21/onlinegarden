import { GET_ALL_TREES } from '../actions/types';

const initialState = {
  isAuthenticated: false,
  user: {},
  userData: {},
  trees: []
};

export default function(state = initialState, action) {
  switch (action.type) {
    case GET_ALL_TREES:
      return {
        ...state,
        trees: action.payload
      };
    default:
      return state;
  }
}