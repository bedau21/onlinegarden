import { combineReducers } from 'redux';
import authReducer from './authReducer'
import userReducer from './userReducer'
import treeReducer from './treeReducer'
import errorReducer from './errorReducer'

export default combineReducers({
    auth: authReducer,
    errors: errorReducer,
    currentUser: userReducer,
    trees: treeReducer
});
