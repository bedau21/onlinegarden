import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { get } from 'lodash'
import { connect } from 'react-redux';
import Header from './header'
import Land from './land'
import Control from './control'
import { getUserData, clearErrors } from '../../actions/userActions';
import './garden.scss'
class Garden extends Component {
  constructor() {
    super();
    this.state = {
      isWatering: false,
      isShoveling: false,
      isGrowing: false,
      isHarvesting: false,
      play: false,
      pause: true,
    }
    this.url = "http://streaming.tdiradio.com:8000/house.mp3"
    this.audio = new Audio(this.url)
  }

  componentDidMount() {
    get(this, 'props.auth.isAuthenticated') ? this.props.getUserData() :
      this.props.history.push('/login')
  }

  watering() {
    this.setState({
      isWatering: !this.state.isWatering,
      isShoveling: false,
      isGrowing: false,
      isHarvesting: false
    });
  }

  harvesting() {
    this.setState({
      isHarvesting: !this.state.isHarvesting,
      isShoveling: false,
      isGrowing: false,
      isWatering: false
    });
  }

  shoveling() {
    this.setState({
      isShoveling: !this.state.isShoveling,
      isWatering: false,
      isGrowing: false,
      isHarvesting: false
    });
  }

  growing() {
    this.setState({
      isGrowing: !this.state.isGrowing,
      isWatering: false,
      isShoveling: false,
      isHarvesting: false
    });
  }

  play = () => {
    this.setState({ play: true, pause: false })
      this.audio.play();
  }
    
  pause = () => {
    this.setState({ play: false, pause: true })
    this.audio.pause();
  }

  setCursor() {
    let cursor = 'grab';
    if (this.state.isHarvesting) {
      cursor = 'grabbing';
    }
    if (this.state.isGrowing) {
      cursor = 'move';
    }
    if (this.state.isWatering) {
      cursor = 'cell';
    }
    if (this.state.isShoveling) {
      cursor = 'not-allowed'
    }
    return cursor
  }

  confirmTable(errors) {
    return errors.message ? <div className="notification-confirm text-center">
      <p>{errors.message}</p>
      <div className="btn btn-success mt-2 delete-button" 
      onClick={this.props.clearErrors}>OK, I see</div> </div> : <div></div>
  }

  render() {
    const { userData } = this.props
    let cursor = this.setCursor()
    return userData ? (
      <div className="garden-background" style={{ cursor }}>
        <Header key={`${userData._id} + 'header'`} 
        onClick={this.play}
        user={userData}></Header>
        <Land key={`${userData._id} + 'land'`}
          trees={userData.trees} user={userData}
          isWatering={this.state.isWatering} watering={this.watering.bind(this)}
          isShoveling={this.state.isShoveling} shoveling={this.shoveling.bind(this)}
          isGrowing={this.state.isGrowing} growing={this.growing.bind(this)}
          isHarvesting={this.state.isHarvesting} harvesting={this.harvesting.bind(this)}
          confirmTable={this.confirmTable.bind(this)}>
        </Land>
        <Control key={`${userData._id} + 'control'`}
          user={userData}
          watering={this.watering.bind(this)}
          shoveling={this.shoveling.bind(this)}
          growing={this.growing.bind(this)}
          harvesting={this.harvesting.bind(this)}>
        </Control>
        {this.confirmTable(this.props.errors)}
      </div>
    ) : <div className="garden-background"></div>
  }
}

Garden.propTypes = {
  getUserData: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired,
  userData: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  auth: state.auth,
  userData: state.currentUser.userData,
  errors: state.errors
});

export default connect(mapStateToProps, { getUserData, clearErrors })(Garden);
