import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { logoutUser } from '../../actions/authActions';

import moneyImage from '../../images/money.png'
import quydeptrai from '../../images/quydeptrai.jpg'

let settingTable = 'setting-table'

class Header extends Component {
  constructor() {
    super();
    this.state = {
      settingStatus: false,
    };
  }
  
  onLogoutClick(e) {
    e.preventDefault();
    this.props.logoutUser()
    window.location.href = '/login'
  }

  toogleSetting() {
    this.setState({
      settingStatus: !this.state.settingStatus
    });
  }

  render() {
    const { user } = this.props
    return (
      <div className="header-wrapper">
        <div className="information">
          <div className="avatar">
            <img className="avatar-image" src={quydeptrai} alt="money"/>
            <p>{user.userName}</p>
          </div>
          <div className="level-and-money">
            <div className="level">
              <div className="column-bar">level {user.level}</div>
            </div>             
            <div className="money">
              <img className="money-image" src={moneyImage} alt="money"/>
              <span>{user.money}</span>
            </div>
          </div> 
        </div>
        <div className="setting">
          <i className="fa fa-music setting-icon" onClick={this.props.play} aria-hidden="true"></i>
          <div className="setting-control setting-icon" onClick={this.toogleSetting.bind(this)}>
            <i className="fa fa-wrench" aria-hidden="true"></i>
            <div className={this.state.settingStatus ? settingTable : `${settingTable} close-table`}>
              <p>User Name: <span>{user.userName}</span></p>
              <p>Email: <span>{user.email}</span></p>
              <p>Level: <span>{user.level}</span></p>
              <p>Money: <span>{user.money}</span></p>
              <div className="btn btn-success" onClick={this.onLogoutClick.bind(this)}>Update Avatar</div>
              <div className="btn btn-success mt-2" onClick={this.onLogoutClick.bind(this)}>Change Password</div>
              <div className="btn btn-warning mt-2" onClick={this.onLogoutClick.bind(this)}>Log Out</div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

Header.propTypes = {
  logoutUser: PropTypes.func.isRequired,
};

export default connect(null, { logoutUser })(Header);