import React, { Component } from 'react';
import { DragSource } from 'react-dnd';
import './garden.scss'
const itemSource = {
  beginDrag(props) {
    return props.item;
  },
  endDrag(props, monitor, component) {
    if (!monitor.didDrop()) {
      return;
    }
    return props.handleDrop(props.item._id);
  }
}

function collect(connect, monitor) {
  return {
    connectDragSource: connect.dragSource(),
    connectDragPreview: connect.dragPreview(),
    isDragging: monitor.isDragging(),
  }
}

class Item extends Component {
  constructor() {
    super();
    this.state = {
    };
  }

  render() {
    const { isDragging, connectDragSource, item, treeURL } = this.props;
    const opacity = isDragging ? 0 : 1;
    return connectDragSource(
      <div className="item" style={{ opacity }}>
        <p className="text-center">{item.name}</p>
        <img className="control-image" src={treeURL} alt="growingTree" />
        <p className="text-center">{item.buyingPrice}$</p>
      </div>
    );
  }
}

export default DragSource('item', itemSource, collect)(Item);