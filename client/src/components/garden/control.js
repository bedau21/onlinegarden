import React, { Component } from 'react';
import Tree from './tree'
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { getAllTrees } from '../../actions/treeAction';
import { addTree } from '../../actions/userActions';

import harvest from '../../images/harvest.png'
import shovel from '../../images/shovel.png'
import wateringCan from '../../images/watering-can.png'
import growingTree from '../../images/growing-tree.png'
let treeTable = 'tree-table'
class Control extends Component {
  constructor() {
    super();
    this.state = {
      treeTableStatus: false
    };
    this.toogleTable = this.toogleTable.bind(this)
  }

  componentDidMount() {
    this.props.getAllTrees()
  }

  toogleTable() {
    this.setState({
      treeTableStatus: !this.state.treeTableStatus
    });
  }
  
  addTree = () => {
  }

  handleChange() {
    this.toogleTable()
    this.props.growing()
  }

  render() {
    const { user, trees } = this.props
    return user && trees ? (
        <div className="control-center">
          <img className="control-image" src={harvest} alt="harvest" onClick={this.props.harvesting}/>
          <img className="control-image" src={shovel} alt="shovel" onClick={this.props.shoveling}/>
          <img className="control-image" src={wateringCan} alt="wateringCan" onClick={this.props.watering}/>
          <div className="control-image growing-tree" onClick={this.handleChange.bind(this)}>
            <img className="control-image" src={growingTree} alt="growingTree" />
            <span>{trees.length}</span>
          </div>
          <div className={this.state.treeTableStatus ? treeTable : `${treeTable} close-table`}>
            {trees && trees.map((item, index) =>
              <Tree key={item._id + index} item={item} treeURL={growingTree} 
              handleDrop={(id) => this.addTree()}/>)}
            <i className="fa fa-times-circle-o" aria-hidden="true" 
            onClick={this.toogleTable}></i>
          </div>
        </div>
      ) : <div className="control-center"></div>
  }
}

Control.propTypes = {
  getAllTrees: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired,
  trees: PropTypes.array.isRequired
};

const mapStateToProps = state => ({
  auth: state.auth,
  trees: state.trees.trees
});

export default connect(mapStateToProps, { getAllTrees, addTree })(Control);