import React, { Component } from 'react';
import { DropTarget } from 'react-dnd';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { addTree, updateTreeStatus, 
  removeUserTree, harvestUserTree } from '../../actions/userActions';
import newTree from '../../images/tree-level-1.png'
import growingTree from '../../images/tree-level-3.png'
import harvestTree from '../../images/tree-level-5.png'

const MAX_LAND_ITEMS = 15
const GROWING_STATUS = 'growing'
const HARVEST_STATUS = 'harvest'

function collect(connect, monitor) {
  return {
    connectDropTarget: connect.dropTarget(),
    hovered: monitor.isOver(),
    item: monitor.getItem(),
  }
}

class Land extends Component {
  constructor(props) {
    super(props);
    this.state = {
      treeTableStatus: false,
      trees: props.trees,
      deleteConfirm: false
    };
  }

  setTreeLevel(status) {
    switch (status) {
      case GROWING_STATUS:
        return growingTree
      case HARVEST_STATUS:
        return harvestTree
      default:
        return newTree;
    }
  }

  static getDerivedStateFromProps(props, state) {
    return { trees: props.trees }
  }

  onDrop(e, userId, treeId, index) {
    e.preventDefault()
    this.props.addTree(userId, treeId, index)
  }

  updateTree(trees) {
    const treeLength = trees.length;
    for (let i = 0; i < MAX_LAND_ITEMS; i++) {
      if (i >= treeLength || this.state.trees[i] === null) {
        this.state.trees.push({})
      }
    }
  }

  treeImage(tree) {
    return tree.status ? <img className="tree-image"
      src={this.setTreeLevel(tree.status)} alt="tree" /> : <div></div>
  }

  handleChange(userId, index, treeId) {
    const { isWatering, isShoveling, isGrowing, isHarvesting } = this.props
    if (isHarvesting) {
      this.props.harvesting()
      this.props.harvestUserTree(userId, index, treeId)
    }
    if (isWatering) {
      this.props.watering()
      this.props.updateTreeStatus(userId, index, (new Date()).getTime())
    }
    if (isShoveling) {
      this.props.shoveling()
      this.props.removeUserTree(userId, index)
    }
    if (isGrowing) {
      this.props.isGrowing()
    }
  }

  landItem(trees, user, item, backgroundColor) {
    const userId = user && user._id
    return trees.map((tree, index) =>
      <div key={index} style={{ backgroundColor }} 
        onClick={this.handleChange.bind(this, userId, index, tree.tree)}
        className="land-item" onDrop={(e) =>
          this.onDrop(e, userId, item._id, index)}>
        {this.treeImage(tree)}
      </div>)
  }

  render() {
    const { connectDropTarget, hovered, item, user } = this.props
    const backgroundColor = hovered ? 'lightgreen' : '#7BC20C'
    let trees = this.state.trees
    this.updateTree(trees)
    return connectDropTarget(
      <div className="land-wrapper">
        {this.landItem(trees, user, item, backgroundColor)}
      </div>
    );
  }
}

Land.propTypes = {
  auth: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  auth: state.auth,
  errors: state.errors
});

const LandCompoent = DropTarget('item', {}, collect)(Land)
export default connect(mapStateToProps, { 
  addTree, updateTreeStatus, removeUserTree, harvestUserTree })(LandCompoent);
