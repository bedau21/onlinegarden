import axios from 'axios';
import { GET_ALL_TREES, GET_ERRORS } from './types';

// Register User
export const getAllTrees = () => dispatch => {
  axios
    .get('/api/trees/all')
    .then(res => {
      dispatch({
        type: GET_ALL_TREES,
        payload: res.data
      })
    })
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      })
    );
};
