import axios from 'axios';
import { GET_ERRORS, GET_USER_DATA, 
  DELETE_TREE_OF_USER, ADD_TREE_TO_USER, CLEAR_ERRORS,
  UPDATE_TREE_STATUS, HARVEST_TREE_OF_USER } from './types';

export const getUserData = () => dispatch => {
  axios
    .get('/api/users/current')
    .then(res => {
      dispatch({
        type: GET_USER_DATA,
        payload: res.data
      })
    })
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      })
    );
};

export const removeUserTree = (userId, index) => dispatch => {
  axios
    .delete(`/api/users/deleteTree/${userId}/${index}`)
    .then(res =>
      dispatch({
        type: DELETE_TREE_OF_USER,
        payload: res.data
      })
    )
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      })
    );
};

export const addTree = (userId, treeId, index) => dispatch => {
  const userData = {userId: userId, index: index, treeId: treeId}
  axios
    .post('/api/users/addTree', userData )
    .then(res =>
      dispatch({
        type: ADD_TREE_TO_USER,
        payload: res.data
      })
    )
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      })
    );
};

export const updateTreeStatus = (userId, index, endTime) => dispatch => {
  const userData = {userId: userId, index: index, endTime: endTime}
  axios
    .post('/api/users/updateTree', userData)
    .then(res => {
      dispatch({
        type: UPDATE_TREE_STATUS,
        payload: res.data
      })
    }
    )
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      })
    );
};

export const harvestUserTree = (userId, index, treeId) => dispatch => {
  const userData = {userId: userId, index: index, treeId: treeId}
  axios
    .post('/api/users/harvestTree', userData)
    .then(res => {
      dispatch({
        type: HARVEST_TREE_OF_USER,
        payload: res.data
      })
    }
    )
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      })
    );
};

// Clear errors
export const clearErrors = () => {
  return {
    type: CLEAR_ERRORS,
    payload: {}
  };
};