const Validator = require('validator')
const isEmpty = require('./is-empty')

module.exports = function validateLoginInput(data) {
    let errors = {}

    data.userName = !isEmpty(data.userName) ? data.userName : ''
    data.password = !isEmpty(data.password) ? data.password : ''
    if (!Validator.isLength(data.userName, {min: 5, max: 30})) {
        errors.userName = 'User name must between 5 and 30 characters'
    }

    if (Validator.isEmpty(data.userName)) {
        errors.userName = 'User name field is required'
    }

    if (Validator.isEmpty(data.password)) {
        errors.password = 'Password field is required'
    }

    return {
        errors,
        isValid: isEmpty(errors)
    }
}