const Validator = require('validator')
const isEmpty = require('./is-empty')

module.exports = function validateTreeInput(data) {
    let errors = {}

    data.name = !isEmpty(data.name) ? data.name : ''
    data.imagePath = !isEmpty(data.imagePath) ? data.imagePath : ''
    if (!Validator.isLength(data.name, {min: 3, max: 30})) {
        errors.name = 'Tree name must between 3 and 30 characters'
    }

    if (Validator.isEmpty(data.name)) {
        errors.name = 'Tree name field is required'
    }

    if (Validator.isEmpty(data.imagePath)) {
        errors.imagePath = 'Image field is required'
    }

    if (Validator.isEmpty(data.buyingPrice)) {
        errors.buyingPrice = 'Buying Price field is required'
    }

    if (Validator.isEmpty(data.sellingPrice)) {
        errors.sellingPrice = 'Selling Price field is required'
    }

    if (Validator.isEmpty(data.numberOfWatering)) {
        errors.numberOfWatering = 'Number of watering to the tree field is required'
    }

    return {
        errors,
        isValid: isEmpty(errors)
    }
}